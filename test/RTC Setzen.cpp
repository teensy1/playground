#include <Arduino.h>
#include <TimeLib.h>
#include <SPI.h>
#include <Wire.h>

// CHAPTER: TFT
#include <Adafruit_GFX.h>
#include <ILI9341_t3n.h>
#include <ili9341_t3n_font_ComicSansMS.h>

#define TFT_IRQ 2 // G0
#define TFT_DC 9  // Device Clock
#define TFT_CS 10 // Chip Select
// #define TFT_RST 8 // Reset

ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC);
// ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC, TFT_RST);
uint8_t test_screen_rotation = 0;

// https://ee-programming-notepad.blogspot.com/2016/10/16-bit-color-generator-picker.html
#define ILI9341_BLACK 0x0000
#define ILI9341_RED 0xF800
#define ILI9341_GREEN 0x07E0
#define ILI9341_WHITE 0xFFFF
#define ILI9341_GREY 0x8410
#define ILI9341_NAVY 0x000F
#define ILI9341_DARKGREEN 0x03E0
#define ILI9341_DARKCYAN 0x03EF
#define ILI9341_MAROON 0x7800
#define ILI9341_PURPLE 0x780F
#define ILI9341_OLIVE 0x7BE0
#define ILI9341_LIGHTGREY 0xC618
#define ILI9341_DARKGREY 0x7BEF
#define ILI9341_BLUE 0x001F
#define ILI9341_CYAN 0x07FF
#define ILI9341_MAGENTA 0xF81F
#define ILI9341_YELLOW 0xFFE0
#define ILI9341_ORANGE 0xFD20
#define ILI9341_GREENYELLOW 0xAFE5
#define ILI9341_PINK 0xF81F
#define BSI 0x0202

/*  code to process time sync messages from the serial port   */
#define TIME_HEADER "T" // Header tag for serial time sync message

unsigned long processSyncMessage() {
    unsigned long pctime = 0L;
    const unsigned long DEFAULT_TIME = 1357041600; // Jan 1 2013

    if(Serial.find(TIME_HEADER)) {
        pctime = Serial.parseInt();
        return pctime;
        if(pctime < DEFAULT_TIME) { // check the value is a valid time (greater
                                    // than Jan 1 2013)
            pctime = 0L; // return 0 to indicate that the time is not valid
        }
    }
    return pctime;
}

void printDigits(int digits) {
    // utility function for digital clock display: prints preceding colon and
    // leading 0
    Serial.print(":");
    if(digits < 10) Serial.print('0');
    Serial.print(digits);
}

void digitalClockDisplay() {
    tft.fillScreen(ILI9341_BLACK);
    // digital clock display of the time
    Serial.print(hour());
    printDigits(minute());
    printDigits(second());
    Serial.print(" ");
    Serial.print(day());
    Serial.print(" ");
    Serial.print(month());
    Serial.print(" ");
    Serial.print(year());
    Serial.println();
    // Datum
    tft.setTextColor(ILI9341_RED);
    tft.setCursor(10, 50);
    tft.print(day());
    tft.setCursor(40, 50);
    tft.print(month());
    tft.setCursor(70, 50);
    tft.print(year());
    // Zeit
    tft.setCursor(10, 100);
    tft.print(hour());
    tft.setCursor(20, 100);
    tft.print(":");
    tft.setCursor(30, 100);
    tft.print(minute());
    tft.setCursor(50, 100);
    tft.print(":");
    tft.setCursor(60, 100);
    tft.print(second());
}

time_t getTeensy3Time() {
    return Teensy3Clock.get();
}

void setup() {

    // CHAPTER TFT
    tft.begin();
    tft.setRotation(3);
    tft.fillWindow(ILI9341_BLACK);
    tft.setTextColor(ILI9341_WHITE);
    tft.setFont(ComicSansMS_12);
    tft.fillRect(0, 0, 320, 240, ILI9341_BLACK);

    // CHAPTER RTC
    // set the Time library to use Teensy 3.0's RTC to keep time
    setSyncProvider(getTeensy3Time);

    Serial.begin(115200);
    while(!Serial && millis() < 4000) { ; }
    delay(100);
    if(timeStatus() != timeSet) {
        Serial.println("Unable to sync with the RTC");
    } else {
        Serial.println("RTC has set the system time");
    }
}

void loop() {
    if(Serial.available()) {
        time_t t = processSyncMessage();
        if(t != 0) {
            Teensy3Clock.set(t); // set the RTC
            setTime(t);
        }
    }
    digitalClockDisplay();
    delay(1000);
}